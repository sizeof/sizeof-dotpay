from setuptools import setup


setup(
    name = 'django-dotpay',
    version = '0.0.1',
    author = 'Mariusz Smenzyk',
    author_email = 'msmenzyk@sizeof.pl',
    license='BSD',
    url = 'https://github.com/sizeof-pl/django-dotpay/',
    packages = ['dotpay', 'dotpay.sms', 'dotpay.payment',  'dotpay.sample'],
)