# -*- encoding: utf-8 -*-
from django.conf import settings


try:
    DOTPAY_SMS_ID = getattr(settings, 'DOTPAY_SMS_ID')
except AttributeError:
    raise BaseException("DOTPAY_SMS_ID is required")