# -*- coding: utf-8 -*-
from urllib import urlopen

from django.forms import ModelForm

from dotpay.sms.settings import DOTPAY_SMS_ID
from dotpay.settings import DOTPAY_ID
from dotpay.sms.models import DotpaySms


class DotpaySMSCheckForm(ModelForm):
    
    class Meta:
        model = DotpaySms
        exclude = ('value',)
    
    def clean(self):
        cleaned_data = self.cleaned_data
        try:
            code = cleaned_data["code"]
        except:
            return cleaned_data
        
        page = urlopen("http://dotpay.pl/check_code_fullinfo.php?id=%s&code=%s&type=sms&del=0&check=%s" % (DOTPAY_ID, DOTPAY_SMS_ID,code)).read()
        page_split = page.split('\n')
        if int(page_split[0]) == 0:
            self._errors["code"] = self.error_class(['Wrong CODE'])
            del cleaned_data["code"]
        elif len(DotSms.objects.filter(code=code)) > 0:
            self._errors["code"] = self.error_class(['CODE was used'])
            del cleaned_data["code"]
        else:
            cleaned_data["value"] = page_split[3]
            self._meta.exclude = None
        return cleaned_data