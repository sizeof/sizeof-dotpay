# -*- coding: utf-8 -*-
from django.db import models

from dotpay.sms.signals import dotpay_sms_ok


class DotpaySms(models.Model):
    code = models.CharField(unique=True, max_length=8, verbose_name='Code', help_text='Authorizaton code')
    value = models.DecimalField(max_digits=5, decimal_places=1, verbose_name='Value', help_text='SMS price net value')
    added = models.DateTimeField('Order date', auto_now_add=True)

    class Meta:
        db_table = 'dotpay_sms'

    def save(self,*args, **kwargs):
        super(DotpaySms, self).save(*args, **kwargs)
        dotpay_sms_ok.send(sender=self)
    
    def __unicode__(self):
        return u"%s : %s" % (self.code, self.value)