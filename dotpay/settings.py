# -*- encoding: utf-8 -*-
from django.conf import settings


try:
    DOTPAY_ID = getattr(settings, 'DOTPAY_ID')
except AttributeError:
    raise BaseException("DOTPAY_ID is required")

DOTPAY_LANG = getattr(settings, 'DOTPAY_LANG', 'pl')

DOTPAY_COUNTRY = getattr(settings, 'DOTPAY_COUNTRY', 'POL')