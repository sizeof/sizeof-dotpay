# -*- coding: utf-8 -*-
from django.contrib import admin
from dotpay.payment.models import DotpayRequest, DotpayResponse
from dotpay.widget import ModelLinkAdminFields


class DotpayResponseInline(admin.StackedInline):
    model = DotpayResponse
    extra = 0
    can_delete = False

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DotpayResponseAdmin(ModelLinkAdminFields, admin.ModelAdmin):
    list_filter = ['t_date', 'status', 't_status', 'dotpay_request__added']
    list_display = ('dotpay_request', 'id', 'status', 'control', 't_id', 't_status', 't_date', 'amount',
                    'orginal_amount', 'email')
    search_fields = ['id', 'status', 't_id', 't_status', 't_date', 'amount', 'orginal_amount', 'email',]
    modellink = ('request', )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class DotpayRequestAdmin(admin.ModelAdmin):
    inlines = [DotpayResponseInline,]
    search_fields = ['opis', 'kwota', 'email', 'added', 'get_status', 'firstname', 'lastname', 'street', 'street_n1',
                     'street_n2', 'city', 'postcode', 'phone']
    list_display = ('opis', 'kwota', 'email', 'added', 'get_status', 'firstname', 'lastname', 'street', 'street_n1',
                    'street_n2', 'city', 'postcode', 'phone')
    list_filter = ['added',]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    
admin.site.register(DotpayRequest, DotpayRequestAdmin)
admin.site.register(DotpayResponse, DotpayResponseAdmin)