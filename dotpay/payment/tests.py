# -*- coding: utf-8 -*-
from datetime import datetime

from django import test
from django import dispatch
from django.test.client import RequestFactory
from django.core.urlresolvers import reverse

from dotpay.payment.models import DotpayResponse, DotpayRequest
from dotpay.settings import DOTPAY_ID


from dotpay.payment.utils import generate_md5
from dotpay.payment.settings import DOTPAY_STATUS_CHOICES, DOTPAY_SERVERS
from dotpay.payment.views import receiver
from dotpay.payment.signals import dotpay_completed, dotpay_canceled, dotpay_refusal, dotpay_new, dotpay_complaint


signal_count = 0

class DotpayTest(test.TestCase):
    def create_request(self):
        self.param = {}
        self.param['kwota'] = str(24.33)
        self.param['opis'] = 'Testowe zamówienie #1'
        self.param['email'] = 'msmenzyk@sizeof.pl'
        self.param['t_id'] = 'TST-20102'

        dotpay_request = DotpayRequest(kwota=self.param['kwota'], opis=self.param['opis'], email=self.param['email'])
        dotpay_request.save(force_insert=True)

        self.control = dotpay_request.control
        self.request = dotpay_request

    def setUp(self):
        self.req = RequestFactory()

    def _post(self, status, t_status, fake=False, user_changed_email=False):
        self.create_request()
        control = self.control
        if fake:
            control += 'fake'

        if user_changed_email:
            email = 'sfdjkgfk@gf.pl'
        else:
            email = self.param['email']

        md5 = generate_md5(control, self.param['t_id'], self.param['kwota'], t_status, email)

        request = self.req.post(reverse('dotpay_receiver'), {'id': DOTPAY_ID,
                                                             'status':status,
                                                             'control':self.control,
                                                             'amount': self.param['kwota'],
                                                             'orginal_amount': self.param['kwota'],
                                                             'email': email,
                                                             'description':self.param['opis'],
                                                             'md5':md5,
                                                             't_id':self.param['t_id'],
                                                             't_status':t_status,
                                                             't_date':datetime.now()})

        request.META['REMOTE_ADDR'] = DOTPAY_SERVERS[0]
        return request

    def testPostwithWrongIP(self):
        request = self._post(1, 1)
        request.META['REMOTE_ADDR'] = '127.0.0.2'
        response = receiver(request)
        self.assertEqual(response.status_code, 403, "CODE: " + str(response.status_code))

    def testResponses(self):
        for stat in DOTPAY_STATUS_CHOICES:
            request = self._post(1, stat[0])
            response = receiver(request)
            self.assertEqual(response.status_code, 200, "CODE: " + str(response.status_code) + " Type: " + stat[1])
        self.assertEqual(DotpayResponse.objects.count(), len(DOTPAY_STATUS_CHOICES))

        global signal_count
        self.assertEqual(len(DOTPAY_STATUS_CHOICES), signal_count)
        signal_count = 0

    def testResponseswithchangedemail(self):
        for stat in DOTPAY_STATUS_CHOICES:
            request = self._post(1, stat[0], False, True)
            response = receiver(request)
            self.assertEqual(response.status_code, 200, "CODE: " + str(response.status_code) + " Type: " + stat[1])
        self.assertEqual(DotpayResponse.objects.count(), len(DOTPAY_STATUS_CHOICES))

        global signal_count
        self.assertEqual(len(DOTPAY_STATUS_CHOICES), signal_count)
        signal_count = 0

    """
    def testFakeResponses(self):
        for stat in DOTPAY_STATUS_CHOICES:
            request = self._post(1, stat[0], True)
            response = receiver(request)
            self.assertEqual(response.status_code, 500, "CODE: " + str(response.status_code) + " Type: " + stat[1])
    """

    def testMultipleResponseToSingleRequest(self):
        request = self._post(1, 1)

        response = receiver(request)
        self.assertEqual(response.status_code, 200, "First transaction should success.")
        self.assertEqual(DotpayResponse.objects.count(), 1)

        response = receiver(request)
        self.assertEqual(response.status_code, 200, "Second transaction should success.")
        self.assertEqual(DotpayResponse.objects.count(), 1, "Secent transaction should not create response.")

        global signal_count
        self.assertEqual(1, signal_count)
        signal_count = 0

@dispatch.receiver(dotpay_canceled)
@dispatch.receiver(dotpay_refusal)
@dispatch.receiver(dotpay_new)
@dispatch.receiver(dotpay_completed)
@dispatch.receiver(dotpay_complaint)


def signals(sender, **kwargs):
    global signal_count
    signal_count += 1