# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DotpayRequest'
        db.create_table('dotpay_request', (
            ('dotpay_request', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('kwota', self.gf('django.db.models.fields.DecimalField')(max_digits=12, decimal_places=2)),
            ('opis', self.gf('django.db.models.fields.TextField')()),
            ('control', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('firstname', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('lastname', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('street_n1', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('street_n2', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('postcode', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('payment', ['DotpayRequest'])

        # Adding model 'DotpayResponse'
        db.create_table('dotpay_response', (
            ('id_response', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('control', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('t_id', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('t_status', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('t_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('orginal_amount', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('md5', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('dotpay_request', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['payment.DotpayRequest'])),
        ))
        db.send_create_signal('payment', ['DotpayResponse'])

        # Adding unique constraint on 'DotpayResponse', fields ['t_id', 'control']
        db.create_unique('dotpay_response', ['t_id', 'control'])


    def backwards(self, orm):
        # Removing unique constraint on 'DotpayResponse', fields ['t_id', 'control']
        db.delete_unique('dotpay_response', ['t_id', 'control'])

        # Deleting model 'DotpayRequest'
        db.delete_table('dotpay_request')

        # Deleting model 'DotpayResponse'
        db.delete_table('dotpay_response')


    models = {
        'payment.dotpayrequest': {
            'Meta': {'object_name': 'DotpayRequest', 'db_table': "'dotpay_request'"},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'control': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'dotpay_request': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'kwota': ('django.db.models.fields.DecimalField', [], {'max_digits': '12', 'decimal_places': '2'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'opis': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'postcode': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'street_n1': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'street_n2': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        'payment.dotpayresponse': {
            'Meta': {'unique_together': "(('t_id', 'control'),)", 'object_name': 'DotpayResponse', 'db_table': "'dotpay_response'"},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'control': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'dotpay_request': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['payment.DotpayRequest']"}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'id_response': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'md5': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'orginal_amount': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            't_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            't_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            't_status': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['payment']