from django import forms
from django.forms import fields

from dotpay.payment.models import DotpayRequest
from dotpay.payment.settings import DOTPAY_URL, DOTPAY_URLC, DOTPAY_BUTTON
from dotpay.settings import DOTPAY_ID, DOTPAY_LANG, DOTPAY_COUNTRY


class DotpayRequestForm(forms.ModelForm):
    id = fields.CharField(initial=DOTPAY_ID, widget=fields.HiddenInput)
    URL = fields.URLField(initial=DOTPAY_URL, widget=fields.HiddenInput)
    URLC = fields.URLField(initial=DOTPAY_URLC, widget=fields.HiddenInput)
    txtguzik = fields.CharField(initial=DOTPAY_BUTTON, widget=fields.HiddenInput)
    typ = fields.IntegerField(initial=0, widget=fields.HiddenInput)
    lang = fields.CharField(max_length=2, initial=DOTPAY_LANG, widget=fields.HiddenInput)
    kraj = fields.CharField(max_length=3, initial=DOTPAY_COUNTRY, widget=fields.HiddenInput)
    dotpay_request = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    firstname = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    lastname = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    email = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    street = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    street_n1 = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    street_n2 = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    city = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    postcode = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)
    phone = fields.CharField(max_length=255, required=False, widget=fields.HiddenInput)

    class Meta:
        model = DotpayRequest
        widgets = {
            'kwota' : fields.HiddenInput,
            'email' : fields.HiddenInput,
            'opis' : fields.HiddenInput,
            'control' : fields.HiddenInput,
            }
    class Meta:
        model = DotpayRequest
        widgets = {
            'kwota' : fields.HiddenInput,
            'email' : fields.HiddenInput,
            'opis' : fields.HiddenInput,
            'control' : fields.HiddenInput,
        }