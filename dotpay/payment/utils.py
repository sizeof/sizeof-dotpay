# -*- coding: utf-8 -*-
from hashlib import md5

from dotpay.settings import DOTPAY_ID
from dotpay.payment.settings import DOTPAY_PIN


def generate_md5(control, t_id, amount, t_status, email):
    """PIN:id:control:t_id:amount:email:service:code:username:password:t_status"""
    list = []
    list.append(DOTPAY_PIN)
    list.append(DOTPAY_ID)
    list.append(control)
    list.append(t_id)
    list.append(amount)
    list.append(email) #email
    list.append("") #service
    list.append("") #code
    list.append("") #username
    list.append("") #password
    list.append(t_status)
    
    list = map(lambda o: str(o), list)
    return md5(":".join(list)).hexdigest()
