# -*- encoding: utf-8 -*-
from django.conf import settings


try:
    DOTPAY_PIN = getattr(settings, 'DOTPAY_PIN')
except AttributeError:
    raise BaseException("DOTPAY_PIN is required")

try:
    DOTPAY_URL  = getattr(settings, 'DOTPAY_URL')
except AttributeError:
    raise BaseException("DOTPAY_URL is required")

try:
    DOTPAY_URLC  = getattr(settings, 'DOTPAY_URLC')
except AttributeError:
    raise BaseException("DOTPAY_URLC is required")

DOTPAY_TRANS_STATUS_CHOICES = (
    ('OK','OK'),
    ('FAIL','FAIL')
)

DOTPAY_STATUS_CHOICE_NEW = '1'
DOTPAY_STATUS_CHOICE_COMPLETED = '2'
DOTPAY_STATUS_CHOICE_REFUSAL = '3'
DOTPAY_STATUS_CHOICE_CANCELED = '4'
DOTPAY_STATUS_CHOICE_COMPLAINT = '5'

DOTPAY_STATUS_CHOICES = (
    (DOTPAY_STATUS_CHOICE_NEW,'NEW'),
    (DOTPAY_STATUS_CHOICE_COMPLETED,'COMPLETED'),
    (DOTPAY_STATUS_CHOICE_REFUSAL, 'REFUSAL'),
    (DOTPAY_STATUS_CHOICE_CANCELED, 'CANCELED/RETURN'),
    (DOTPAY_STATUS_CHOICE_COMPLAINT,'COMPLAINT'),
)

DOTPAY_SERVERS  = getattr(settings, 'DOTPAY_SERVERS', ['195.150.9.37'])

DOTPAY_BUTTON  = getattr(settings, 'DOTPAY_BUTTON', 'Powrót do sklepu')