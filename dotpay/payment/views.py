# -*- coding: utf-8 -*-
import urlparse
import logging

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from dotpay.payment.models import DotpayResponse, DotpayRequest
from dotpay.payment.settings import DOTPAY_SERVERS


log = logging.getLogger(__name__)

@csrf_exempt
def receiver(request):
    log.info("Receive start")
    log.info("DOTPAY_SERVERS: %s" % DOTPAY_SERVERS)
    log.info("REMOTE ADDR: %s" % request.META['REMOTE_ADDR'])
    log.info("Log full request:")
    log.info(request)

    if request.POST:
        if request.META['REMOTE_ADDR'] in DOTPAY_SERVERS:
            vars = {}
            try:
                post = urlparse.parse_qs(request._raw_post_data)
            except AttributeError:
                post = request.POST
            log.info("POST data")
            log.info(dict(post))
            for field in DotpayResponse._meta.fields:
                if 'id_response' != field.name and field.name != 'dotpay_request':
                    var = post.get(field.name, None)
                    if var:
                        if type(var) == list:
                            var = var[0].decode("utf-8")
                        elif type(var) == str:
                            var = var.decode("utf-8")
                    vars[field.name] = var
            log.info("vars data")
            log.info(dict(vars))
            try:
                control = vars.pop('control')
                dotpay_request = DotpayRequest.objects.get(control=control)
                dotpay_response = DotpayResponse.objects.get_or_create(control=control,
                    t_id=vars.pop('t_id'), dotpay_request=dotpay_request, defaults=vars)
                log.info("dotpay response")
                log.info(dotpay_response[0].__dict__)
            except Exception, e:
                print e
                log.info("Exception, code: 500")
                log.info(str(e))
                return HttpResponse("ERR", status=500)
            else:
                log.info("OK, code: 200")
                return HttpResponse("OK", status=200)
        else:
            log.info("Forbidden, code: 403")
            return HttpResponse("ERR", status=403)
    else:
        log.info("405 Method Not Allowed, code: 405")
        return HttpResponse("405 Method Not Allowed", status=405)