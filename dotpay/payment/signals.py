from django.dispatch import Signal


dotpay_completed = Signal()
dotpay_new = Signal()
dotpay_refusal = Signal()
dotpay_canceled = Signal()
dotpay_complaint = Signal()
dotpay_error = Signal()