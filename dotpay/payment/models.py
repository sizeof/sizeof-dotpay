# -*- coding: utf-8 -*-
import random
import time

from hashlib import md5
from django.template.defaultfilters import slugify
from django.db import models
from dotpay.payment.utils import generate_md5
from dotpay.payment.signals import dotpay_canceled, dotpay_error, dotpay_new, dotpay_refusal, dotpay_complaint, dotpay_completed
from dotpay.payment.settings import  DOTPAY_TRANS_STATUS_CHOICES, DOTPAY_STATUS_CHOICES, DOTPAY_STATUS_CHOICE_NEW, \
    DOTPAY_STATUS_CHOICE_COMPLETED, DOTPAY_STATUS_CHOICE_REFUSAL, DOTPAY_STATUS_CHOICE_CANCELED, DOTPAY_STATUS_CHOICE_COMPLAINT


class DotpayRequest(models.Model):
    dotpay_request = models.AutoField(primary_key=True)

    kwota = models.DecimalField(max_digits=12, decimal_places=2, help_text='The amount of the transaction given the hundredth part.')
    opis = models.TextField(help_text='Description of transactions.')
    control = models.CharField(max_length=128, help_text='Control parameter', unique=True)
    added = models.DateTimeField(verbose_name='Order date', auto_now_add=True, help_text='Order date')
    email = models.EmailField()

    firstname = models.CharField(max_length=255, null=True, blank=True)
    lastname = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    street = models.CharField(max_length=255, null=True, blank=True)
    street_n1 = models.CharField(max_length=255, null=True, blank=True)
    street_n2 = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=255, null=True, blank=True)
    postcode = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        db_table = 'dotpay_request'

    def __unicode__(self):
        return u"[%s|%s] %s" % (self.added, self.kwota, unicode(self.opis))
    
    def save(self,*args, **kwargs):
        self.gen_control()
        super(DotpayRequest, self).save(*args, **kwargs)
        
    def get_status(self):
        if self.dotpayresponse_set.count() > 0:
            status = self.dotpayresponse_set.order_by("-t_status").all()[0]
            return status.get_t_status_display()
        else:
            return 'NEW'
    
    def gen_control(self):
        self.control = md5(( str(self.kwota) + slugify(self.opis) + str(random.randint(0, 99999)) + str(time.time()) )).hexdigest()


class DotpayResponse(models.Model):
    id_response = models.AutoField(primary_key=True)
    id = models.PositiveIntegerField(help_text='Account ID in the system Dotpay, for which payment is made (ID Account Reseller)')
    status = models.CharField(max_length=4, choices=DOTPAY_TRANS_STATUS_CHOICES,help_text='Information on the possible occurrence of errors on the site Dotpay.')
    control = models.CharField(max_length=128, help_text='Control parameter if specified in transfer to the buyer Dotpay')
    t_id = models.CharField(max_length=100, help_text='Identification number assigned to the accounting for transactions in your account Dotpay (Sellers).')
    t_status = models.CharField(max_length=1, choices=DOTPAY_STATUS_CHOICES)
    t_date = models.DateTimeField(auto_now_add=True, help_text='Date of receipt of the message')
    amount = models.DecimalField(max_digits=5, decimal_places=2, help_text='The amount of the transaction. Decimal separator is a full stop')
    orginal_amount = models.CharField(max_length=100, help_text='The amount of the transaction and the currency marker (separated by spaces). Decimal separator is a full stop')
    email = models.CharField(max_length=100, help_text='E-mail address of the person making the payment.')
    description = models.CharField(max_length=255, null=True, blank=True, help_text='The full description of the transaction')
    md5 = models.CharField(max_length=32)
    dotpay_request = models.ForeignKey(DotpayRequest)

    class Meta:
        db_table = 'dotpay_response'
        unique_together = ('t_id', 'control')
    
    def __unicode__(self):
        return u"[%s] - %s" % (self.t_status, self.dotpay_request.opis)

    def gen_md5(self):
        return generate_md5(self.control, self.t_id, self.amount, self.t_status, self.email)

    def check_md5(self):
        return bool(self.gen_md5() == self.md5)
    
    def save(self,*args, **kwargs):
        self.request = DotpayRequest.objects.get(control=self.control)
        if self.check_md5():
            super(DotpayResponse, self).save(*args, **kwargs)
            if self.t_status == DOTPAY_STATUS_CHOICE_COMPLETED:
                dotpay_completed.send(sender=self)
            elif self.t_status == DOTPAY_STATUS_CHOICE_NEW:
                dotpay_new.send(sender=self)
            elif self.t_status == DOTPAY_STATUS_CHOICE_REFUSAL:
                dotpay_refusal.send(sender=self)
            elif self.t_status == DOTPAY_STATUS_CHOICE_CANCELED:
                dotpay_canceled.send(sender=self)
            elif self.t_status == DOTPAY_STATUS_CHOICE_COMPLAINT:
                dotpay_complaint.send(sender=self)
            else:
                dotpay_error.send(sender=self)
                raise BaseException("T_STATUS not implemented: %s" % str(self.t_status))
        else:
            dotpay_error.send(sender=self)
            raise BaseException("MD5 IS INCORRECT - [id_response: %s,  dotpay_id: %s]" % (self.id_response, self.id))
