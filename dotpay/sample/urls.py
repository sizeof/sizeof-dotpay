from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'form/$','dotpay.sample.order.views.form'),
    (r'form/status/$','dotpay.sample.order.views.status'),
    (r'form-sms/','dotpay.sample.sms.views.form'),
)