# coding=utf-8
import random
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt

from dotpay.payment.models import DotpayRequest
from dotpay.payment.forms import DotpayRequestForm
from dotpay.sample.order.models import Order


@cache_control(no_store=True, no_cache=True, must_revalidate=True, post_check=0, pre_check=0, max_age=0)
def form(request):

    test_id = random.randint(1, 99999)

    dotpay_request = DotpayRequest.objects.create(
        kwota=24.99,
        opis='Testowe zamówienie %s' % test_id,
        email='msmenzyk-%s@sizeof.pl' % test_id,
        firstname = 'Mariusz',
        lastname = 'Smenżyk',
        street = 'ul. Jakas',
        street_n1 = '28',
        street_n2 = '4A',
        city = 'Kraków',
        postcode = '33-300',
        phone = '775-447-778'
    )

    Order.objects.create(request=dotpay_request, user_id=1)
    form = DotpayRequestForm(instance=dotpay_request)

    return render_to_response('form.html', { 'form': form, 'dotpay_request': dotpay_request },
        context_instance=RequestContext(request))


@csrf_exempt
def status(request):
    return render_to_response('status.html', {'status': request.GET.get('status')}, context_instance=RequestContext(request))