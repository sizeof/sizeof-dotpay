from django.contrib.auth.models import User
from django.db import models

from dotpay.payment.models import DotpayRequest


class Order(models.Model):
    user = models.ForeignKey(User)
    request = models.OneToOneField(DotpayRequest)

    class Meta:
        db_table = 'dotpay_sample_order'