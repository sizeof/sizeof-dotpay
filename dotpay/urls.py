from django.conf.urls.defaults import patterns, url, include


urlpatterns = patterns('',
    url(r'^receiver/', 'dotpay.payment.views.receiver', name="dotpay_receiver"),
    url(r'^sample/', include('dotpay.sample.urls')),
)